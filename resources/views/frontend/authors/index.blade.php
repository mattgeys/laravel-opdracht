@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <div class="col-md-4">
                    <h2>Auteurs</h2>
                </div>
                <div class="col-md-4">
                    <a href='/auteurs/toevoegen' class="btn btn-primary pull-right">Auteur toevoegen</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                @if(!$authors->isEmpty())
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Voornaam</th>
                            <th>Achternaam</th>
                        </tr>
                        @foreach($authors as $author)
                            <tr>
                                <td>{{$author->firstname}}</td>
                                <td>{{$author->lastname}}</td>
                                <td><a href="/auteurs/{{$author->id}}/bewerken">Bewerken</a></td>
                                <td><a href="/auteurs/{{$author->id}}/verwijderen">Verwijderen</a></td>
                            <tr>
                        @endforeach
                        </thead>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection