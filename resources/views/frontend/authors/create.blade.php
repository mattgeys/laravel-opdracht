@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <h2>Auteur toevoegen</h2>

                <form method="POST" action="{{url('/auteurs')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="firstname">Voornaam: *</label>
                        <input type="text" class="form-control" name="firstname">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Achternaam: *</label>
                        <input type="text" class="form-control" name="lastname">
                    </div>
                    <button type="submit" class="btn btn-primary">Toevoegen</button>
                </form>
            </div>
        </div>
    </div>
@endsection