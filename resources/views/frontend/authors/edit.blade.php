@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <h2>Auteur bewerken</h2>

                <form method="POST" action="{{url('/auteurs/'.$author->id)}}">
                    {{csrf_field()}}
                    {{method_field('PATCH')}}
                    <div class="form-group">
                        <label for="firstname">Voornaam: *</label>
                        <input type="text" class="form-control" name="firstname" value="{{$author->firstname}}">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Achternaam: *</label>
                        <input type="text" class="form-control" name="lastname" value="{{$author->lastname}}">
                    </div>
                    <button type="submit" class="btn btn-success">Bewerken</button>
                </form>
            </div>
        </div>
    </div>
@endsection