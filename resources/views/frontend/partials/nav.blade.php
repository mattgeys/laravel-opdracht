<nav id='navbar' class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{Request::is('/') ? 'active' : ''}}"><a href="/">Home</a></li>
                <li class="{{Request::is('boeken') ? 'active' : ''}}"><a href="/boeken">Boeken</a></li>
                <li class="{{Request::is('auteurs') ? 'active' : ''}}"><a href="/auteurs">Auteurs</a></li>
                <li class="{{Request::is('genres') ? 'active' : ''}}"><a href="/genres">Genres</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>