@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <div class="col-md-4">
                    <h2>Genres</h2>
                </div>
                <div class="col-md-4">
                    <a href='/genres/toevoegen' class="btn btn-primary pull-right">Genre toevoegen</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                @if(!$genres->isEmpty())
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Naam</th>
                        </tr>
                        @foreach($genres as $genre)
                            <tr>
                                <td>{{$genre->name}}</td>
                                <td><a href="/genres/{{$genre->id}}/bewerken">Bewerken</a></td>
                                <td><a href="/genres/{{$genre->id}}/verwijderen">Verwijderen</a></td>
                            <tr>
                        @endforeach
                        </thead>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection