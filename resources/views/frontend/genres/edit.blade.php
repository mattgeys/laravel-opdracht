@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <h2>Genre bewerken</h2>

                <form method="POST" action="{{url('/genres/'.$genre->id)}}">
                    {{csrf_field()}}
                    {{method_field('PATCH')}}
                    <div class="form-group">
                        <label for="name">Naam: *</label>
                        <input type="text" class="form-control" name="name" value="{{$genre->name}}">
                    </div>
                    <button type="submit" class="btn btn-success">Bewerken</button>
                </form>
            </div>
        </div>
    </div>
@endsection