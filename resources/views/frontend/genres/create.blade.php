@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <h2>Genre toevoegen</h2>

                <form method="POST" action="{{url('/genres')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Naam: *</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <button type="submit" class="btn btn-primary">Toevoegen</button>
                </form>
            </div>
        </div>
    </div>
@endsection