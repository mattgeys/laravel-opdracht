@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <h2>Boek bewerken</h2>

                <form method="POST" action="{{url('/auteurs/'.$book->id)}}">
                    {{csrf_field()}}
                    {{method_field('PATCH')}}
                    <div class="form-group">
                        <label for="title">Titel: *</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    @if(!$genres->isEmpty())
                        <div class="form-group">
                            <label for="genre">Genre: *</label>
                            <div class="col-sm-offset-1">
                                @foreach($genres as $genre)
                                    <input class="col-sm-offset-1" type="checkbox" name="genre"
                                           value="{{$genre->name}}"> {{$genre->name}}</br>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(!$authors->isEmpty())
                        <div class="form-group">
                            <label for="author">Auteur: *</label>
                            <select class="form-control" name="author">
                                @foreach($authors as $author)
                                    <option>{{$author->firstname}} {{$author->lastname}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                    <button type="submit" class="btn btn-success">Bewerken</button>
                </form>
            </div>
        </div>
    </div>
@endsection