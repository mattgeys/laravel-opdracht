@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('frontend.partials.errors')
                <h2>Boeken</h2>
            </div>
            <div class="col-lg-4">
                <a href='/boeken/toevoegen' class="btn btn-primary pull-right">Boek toevoegen</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                @if(!$books->isEmpty())
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Genres</th>
                            <th>Auteur</th>
                        </tr>
                        @foreach($books as $book)
                            <tr>
                                <td>{{$book->title}}</td>
                                <td>{{$book->title}}</td>
                                <td>{{$book->title}}</td>
                                <td><a href="/boeken/{{$book->id}}/bewerken">Bewerken</a></td>
                                <td><a href="/boeken/{{$book->id}}/verwijderen">Verwijderen</a></td>
                            <tr>
                        @endforeach
                        </thead>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection