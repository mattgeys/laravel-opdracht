@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                @include('frontend.partials.errors')
                <h2>Boek toevoegen</h2>

                <form method="POST" action="{{url('/boeken')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">Titel: *</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    @if(!$genres->isEmpty())
                        <div class="form-group">
                            <label for="genre[]">Genre: *</label>
                            <div class="col-sm-offset-1">
                                @foreach($genres as $genre)
                                    <input class="col-sm-offset-1" type="checkbox" name="genre[]"
                                           value="{{$genre->id}}"> {{$genre->name}}</br>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>Er zijn nog geen genres toegevoegd. <a href="/genres/toevoegen">Genre toevoegen</a></p>
                        </div>
                    @endif

                    @if(!$authors->isEmpty())
                        <div class="form-group">
                            <label for="author">Auteur: *</label>
                            <select class="form-control" name="author">
                                @foreach($authors as $author)
                                    <option value="{{$author->id}}">{{$author->firstname}} {{$author->lastname}}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <p>Er zijn nog geen auteurs toegevoegd. <a href="/auteurs/toevoegen">Auteur toevoegen</a>
                            </p>
                        </div>
                    @endif
                    <button type="submit" class="btn btn-primary">Toevoegen</button>
                </form>
            </div>
        </div>
    </div>
@endsection