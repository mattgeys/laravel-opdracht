<html>
<head>
    <meta charset="utf-8">
    <title>Boekencollectie</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css">
    @yield('css')
</head>
<body>
<div class="container">
    <div class="jumobtron text-left">
        <div class="container">
            <h1>Boekencollectie</h1>
            <p class="lead">By Geysen Matthias</p>
        </div>
    </div>
</div>
@include('frontend.partials.nav')
@yield('content')
@yield('scripts')
</body>
</html>
