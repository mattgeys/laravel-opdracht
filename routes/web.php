<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('frontend.home');
});


//Books
Route::get('/boeken', 'BooksController@index');
Route::get('/boeken/toevoegen', 'BooksController@create');
Route::get('/boeken/{id}/bewerken', 'BooksController@edit');
Route::get('/boeken/{id}/verwijderen', 'BooksController@destroy');

Route::post('/boeken', 'BooksController@store');
Route::patch('/boeken/{id}', 'BooksController@update');

//Authors
Route::get('/auteurs', 'AuthorsController@index');
Route::get('/auteurs/toevoegen', 'AuthorsController@create');
Route::get('/auteurs/{id}/bewerken', 'AuthorsController@edit');
Route::get('/auteurs/{id}/verwijderen', 'AuthorsController@destroy');

Route::post('/auteurs', 'AuthorsController@store');
Route::patch('/auteurs/{id}', 'AuthorsController@update');

//Genres
Route::get('/genres', 'GenresController@index');
Route::get('/genres/toevoegen', 'GenresController@create');
Route::get('/genres/{id}/bewerken', 'GenresController@edit');
Route::get('/genres/{id}/verwijderen', 'GenresController@destroy');

Route::post('/genres', 'GenresController@store');
Route::patch('/genres/{id}', 'GenresController@update');
